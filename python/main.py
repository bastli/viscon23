import flipdot
import time

flipdot.start()
flipdot.clear()
flipdot.refresh()

for i in range(0, 16*40):

    flipdot.refresh()
    if i > 0:
        flipdot.set_pixel((i-1)%40, int((i-1)/40), False)
    flipdot.set_pixel(i%40, int(i/40))
    flipdot.refresh()
    time.sleep(100/(16*40))

flipdot.stop()
