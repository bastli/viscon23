import socket
import time
import numpy

from threading import Thread, Lock

UDP_IP = "esp-flipdot.bastli.ethz.ch"
UDP_PORT = 3333
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

class Flipdot:
    def __init__(self):

        self.frame = [
        [
            0b00000000001111111111,
            0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
        ], [
            0b00000000001111111111,
            0b11111111110000000000,
            0b00000000001111111111,
            0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
        ]]

        self.new_frame = [
        [
            0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
        ], [
            0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
	    0b00000000001111111111,
	    0b11111111110000000000,
        ]]

        self.lock = Lock()
        self.frame_update = 0
        self.running = True

    def send_frame(self, *args):
        mystring = b''
        for i in range(0, 2):
            for k in range(0, 16):
                mystring += self.frame[i][k].to_bytes(4, 'little')
        sock.sendto(mystring, (UDP_IP, UDP_PORT))
        #print(mystring)

    def update_frame(self, *args):
        while self.running:
            with self.lock:
                self.send_frame()
            time.sleep(1.5)


flipdot = Flipdot()

def start():

    flipdot.frame_update = Thread(target=flipdot.update_frame, args=())
    print("Starting the sending loop ...")
    flipdot.frame_update.start()

def stop():

    if flipdot.running and flipdot.frame_update != 0:
        flipdot.running = False
        print("Waiting for the thread to join ...")
        flipdot.frame_update.join()


def set_pixel(x, y, b=True):
    if 40 > x >= 0 and 0 <= y < 16:
        if b:
            flipdot.new_frame[int(x/20)][y] |= (1 << 19-(x % 20))
        else:
            flipdot.new_frame[int(x/20)][y] &= ~(1 << 19-(x % 20))

def clear():
    for i in range(0, 2):
        for k in range(0, 16):
            flipdot.new_frame[i][k] = 0

def refresh():
    with flipdot.lock:
        h = flipdot.frame
        flipdot.frame = flipdot.new_frame
        flipdot.new_frame = h
