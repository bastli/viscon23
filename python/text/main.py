import socket
import time

import font
import numpy

UDP_IP = "esp-flipdot.bastli.ethz.ch"
UDP_PORT = 3333
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

MESSAGE = b"Hello, World!"

frame = [
    [
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
    ], [
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
        0b00000000001111111111,
        0b11111111110000000000,
    ]]


def calculate_text(text):
    length = len(text)
    offset = (5 - length) * 4

    for i in range(0, length):
        draw_character(offset + i * 8, ord(text[i]))


def draw_character(position, character):
    fonty = font.Font()
    for i in range(0, 8):
        line = fonty.get_line(character, i)
        for k in range(0, 8):
            if (line & (1 << k)) != 0:
                set_pixel(position + i, 4 + k)

def send_frame():
    mystring = b''
    for i in range(0, 2):
        for k in range(0, 16):
            mystring += frame[i][k].to_bytes(4, 'little')
    sock.sendto(mystring, (UDP_IP, UDP_PORT))

def set_pixel(x, y):
    if 40 > x >= 0 and 0 <= y < 16:
        frame[int(x/20)][y] |= (1 << 19-(x % 20))

def clear():
    for i in range(0, 2):
        for k in range(0, 16):
            frame[i][k] = 0

while True:
    clear()
    calculate_text("Test")
    send_frame()
    time.sleep(1)
    clear()
    calculate_text("1234")
    send_frame()
    time.sleep(1)
