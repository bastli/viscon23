This repository gives you the possibility to interact with the Baslti Flipdot Display.

Just select one of the available languages and use the
 **clear(void)**  **set_pixel(int x, int y)**  **refresh(void)**
methods to control the pixels on the display

The Python Flipdot script uses a double-buffered frame and is updated each 1.5 second. Be careful, because the framebuffers are flipped on every call of the **refresh** method.

Use the **clear** method to blank the whole frame and the **set_pixel** method to flip pixels.
