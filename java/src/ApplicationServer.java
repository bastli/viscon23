package server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

public class ApplicationServer extends Thread implements Runnable {

	HttpServer server;

	public static void main(String[] args) {
		new ApplicationServer();
	}

	public ApplicationServer() {
		this.run();
	}

	@Override
	public void run() {
		super.run();
		try {
			InetSocketAddress address = new InetSocketAddress("localhost", 8080);
			server = HttpServer.create(address, 5);
			server.createContext("/", new MyHttpHandler(this));
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}


class MyHttpHandler implements HttpHandler{

	FlipdotClient client;

	boolean socket_active = false;
	String current_text = "";

	int mode = -1;

	ApplicationServer java_server;
	String charset = "ISO-8859-1";

	public MyHttpHandler(ApplicationServer server) {
		java_server = server;
	}

	@Override
	public void handle(HttpExchange arg0) throws IOException {
		String requestParamValue = null;
		if(arg0.getRequestMethod().equals("GET")) {
//			System.out.println(arg0.getRequestURI().toString());
		}
		else if(arg0.getRequestMethod().equals("POST")) {
			requestParamValue = handlePost(arg0);
		}
		else {
//			System.out.println("Don't know how to handle this request");
		}
		handleResponse(arg0);

	}

	private String handlePost(HttpExchange exchange) {
		String input = "";
		String text;
		InputStream in = exchange.getRequestBody();
		InputStreamReader reader;
		try {
			reader = new InputStreamReader(in, Charset.forName(charset));
			while(reader.ready()) {
				input += (char)reader.read();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
//		handle the post request by the current mode, then by priority
		switch (mode){

		case -1:	//start page with start and shutdown server
			if(input.contains("startSocket")) {
				System.out.println("Start connection to flipdot display.");
				client = new FlipdotClient();
				socket_active = client.startConnection();
				if(socket_active) ++mode;
			}

			else if(input.contains("shutdown")) {
				System.out.println("Shutdown this Java Webserver.");
				java_server.server.stop(0);
			}

			break;

		case 0: 	//index page with submenus dots, text, gif, clock, ampel
			if(input.contains("stopSocket")) {
				client.closeConnection();
				client.stop();
				mode = -1;
				client.setMode(-1);
			}
			else if(input.contains("dots")) {
				mode = 1;
				client.setMode(0);
			}
			else if(input.contains("text")) {
				mode = 2;
				client.setMode(1);
			}
			else if(input.contains("img")) {
				mode = 3;
				client.setImage();
				client.setMode(2);
			}
			else System.out.println("unknown button");
			break;

		case 1:
			if(input.contains("return")) {
				mode = 0;
			}
			else {
			//	client.setDots(input);
			//	client.refresh();
			}
			break;

		case 2:
			if(input.contains("return")) mode = 0;
			else if(input.contains("sendText")) {
				if(input.contains("textField")) {
					current_text = input.substring(input.indexOf("=") + 1, input.indexOf("&"));
				}
				client.setText(current_text);
			}
			break;
		case 3:
			if(input.contains("return")) mode = 0;
			else if(input.contains("form-data")) {
				//client.loadGif();
				make_temp_file(input);
				client.setImage("/home/bastli/temp.dat");
			}
			break;
		}

		return input;
	}

	private void handleResponse(HttpExchange exchange) {

		OutputStream outputStream = exchange.getResponseBody();
		StringBuffer htmlResponse = new StringBuffer();

		htmlResponse.append("<html>\n");
		htmlResponse.append("<body>\n");
		htmlResponse.append("\t<h1>Benutze dieses Interface, um das Flipdot Display zu steuern.</h1>\n");
		if(mode == -1) {
			htmlResponse.append("\t<form method=\"POST\">\n");
			htmlResponse.append("\t\t<input type=\"submit\" name=\"startSocket\" class=\"button\" value=\"Start\" />\n");
			htmlResponse.append("\t\t<input type=\"submit\" name=\"shutdown\" class=\"button\" value=\"Shutdown\" />\n");
			htmlResponse.append("\t</form>\n");
		}
		else if (mode == 0){
			htmlResponse.append("\t<form method=\"POST\">\n");
			htmlResponse.append("\t\t<div height=\"40px\">\n");
			htmlResponse.append("\t\t\t<input type=\"submit\" height=\"40px\" width=\"100px\" name=\"dots\" class=\"button\" value=\"Dots\" />\n");
			htmlResponse.append("\t\t\t<input type=\"submit\" height=\"40px\" width=\"100px\" name=\"text\" class=\"button\" value=\"Text\" />\n");
			htmlResponse.append("\t\t\t<input type=\"submit\" height=\"40px\" width=\"100px\" name=\"img\" class=\"button\" value=\"GIF\" />\n");
			htmlResponse.append("\t\t</div>\n");

			htmlResponse.append("\t\t<input type=\"submit\" name=\"stopSocket\" class=\"button\" value=\"Stop\" />\n");
			htmlResponse.append("\t</form>\n");
		}
		else if (mode == 1) {
			htmlResponse.append("\t<form method=\"POST\">\n");
			for(int i = 0; i < 16; ++i) {
				for(int k = 0; k < 40; ++k) {
					htmlResponse.append("\t\t<input type=\"checkbox\" onChange=\"this.form.submit()\" name=\"chbx\" value=\"");
					htmlResponse.append(40 * i + k);
					//if(!client.isWhite(k, i)) {
					//	htmlResponse += "\" checked=\"";
					//	htmlResponse += "true";
					//}
					htmlResponse.append("\" />\n");
				}
				htmlResponse.append("\t\t<br>\n");
			}
			htmlResponse.append("<br>\n");
			htmlResponse.append("\t\t<input type=\"submit\" name=\"return\" class=\"button\" value=\"Zurueck\" />\n");
			htmlResponse.append("\t</form>\n");
		}
		else if(mode == 2) {
			htmlResponse.append("\t<form method=\"POST\">\n");
			htmlResponse.append("\t\t<input type=\"text\" name=\"textField\" >\n");
			htmlResponse.append("\t\t<input type=\"submit\" name=\"sendText\" value=\"Send Text\" />\n");
			htmlResponse.append("<br>\n");
			htmlResponse.append("\t\t<input type=\"submit\" name=\"return\" class=\"button\" value=\"Zurueck\" />\n");
			htmlResponse.append("\t</form>\n");
		}
		else if(mode == 3) {
			htmlResponse.append("\t<form method=\"POST\">\n");
			htmlResponse.append("\t\t<input type=\"submit\" name=\"return\" class=\"button\" value=\"Zurueck\" />\n");
			htmlResponse.append("\t</form>\n");

			htmlResponse.append("\t<form enctype=\"multipart/form-data\" method=\"POST\">\n");
			htmlResponse.append("\t\tFile: <input type=\"file\" name=\"filename\"/>\n");
			htmlResponse.append("\t\t<input type=\"submit\"/>\n");
			htmlResponse.append("\t</form>\n");

		}

		htmlResponse.append("</body>\n");
		htmlResponse.append("</html>");
		try {
			exchange.sendResponseHeaders(200, htmlResponse.length());
			outputStream.write(htmlResponse.toString().getBytes());
			outputStream.flush();
			outputStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void make_temp_file(String input) {
		File f = new File("/home/bastli/temp.dat");
		FileOutputStream fw;
		OutputStreamWriter writer;
		f.deleteOnExit();
		int i = 0;
		String boundary = "";
		String[] lines = input.split("\n");

		try {
			fw = new FileOutputStream(f);
			writer = new OutputStreamWriter(fw, Charset.forName(charset));
			boundary = lines[0];
//			System.out.println("found boundary");
			while(i < lines.length) {
				++i;
				if(lines[i].contains("Content-Type:"))break;
			}
			//now the index i is two lines before the file begins
			i += 2;
//			System.out.println("found file begin");
			for(;i < lines.length - 1; ++i) {
				for(int k=0; k < lines[i].length(); ++k) {
					char c = lines[i].charAt(k);
//					System.out.println(c + " " + (int)c);
					writer.write(c);
				}
				if(isBoundary(boundary, lines[i+1])) break;
				writer.write('\n');
			}
//			System.out.println("completed copy");

			writer.flush();
			writer.close();

//			System.out.println("Loaded successfully");

		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

	private boolean isBoundary(String boundary, String b) {

		for(int i=0; i < boundary.length(); ++i) {
			if(boundary.charAt(i) != b.charAt(i)) {
				return false;
			}
		}
		return true;
	}
}
